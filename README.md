# Anyprobe customer portal

The client facing portal web UI for managing anyprobe devices.

Aims to be a simple bootstrap based UI which syncs to and from
the back-end (packrat) API.
